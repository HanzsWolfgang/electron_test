const electron = require('electron');
const {app, Menu, BrowserWindow} = electron;

let fs = require('fs');
let win;

function createWindow() {

	let window_width = 960;
	let window_height = 700;

	// Create the browser window.
	let win = new BrowserWindow({
		width:  window_width,
		height: window_height,
		icon: __dirname + '/resources/favicon.png'
	});

    // Create the URL - This is the index.html file in this directory
    let url = require('url').format({
        protocol: 'file',
        slashes: true,
        pathname: require('path').join(__dirname, 'index.html')
    });

	// and load the index.html of the app.
	win.loadURL(url);

	// Emitted when the window is closed.
	win.on('closed', () => {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		win = null;
	});
    //
    // const template = [
    //     {
    //         label: 'Edit',
    //         submenu: [
    //             {
    //                 role: 'undo'
    //             }
    //         ]
    //     }
    // ]
    //
    // const menu = Menu.buildFromTemplate(template);
    // Menu.setApplicationMenu(menu);

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On macOS it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (win === null) {
		createWindow();
	}
});
